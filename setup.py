# -*- coding: utf-8 -*-
"""Packaging logic for Flake8-JSON."""
import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'src'))  # noqa

import flake8_codeclimate_gitlab
import setuptools

setuptools.setup(
    version=flake8_codeclimate_gitlab.__version__,
    package_dir={
        '': 'src/',
    },
)
