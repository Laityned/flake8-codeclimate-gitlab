"""Module containing all of the JSON reporters for Flake8."""
from __future__ import print_function, unicode_literals

import hashlib
import json
import time
from flake8.formatting import base

from .variables import ERROR_CLASSES, SEVERITY_CLASSES

class CodeclimateGitlab(base.BaseFormatter):
    """Formatter for CodeClimate JSON."""

    def after_init(self):
        """Force newline to be empty."""
        self.first_written = False
        self.newline = ""

    def write_line(self, line):
        """Override write for convenience."""
        self.write(line, None)

    def start(self):
        """Override the default to start printing JSON."""
        super(CodeclimateGitlab, self).start()
        self.write_line("[")

    def stop(self):
        """Override the default to finish printing JSON."""
        self.write_line("]")

    @staticmethod
    def _fingerprint(violation):
        return hashlib.md5(
            "{}:{}:{}".format(
                violation.filename,
                violation.code,
                violation.line_number,
            ).encode()
        ).hexdigest()

    @staticmethod
    def _get_category(violation_code):
        """Convert a Violation.code to a category"""
        for i in range(4, 0, -1):
            if violation_code[:i] in ERROR_CLASSES:
                return ERROR_CLASSES[violation_code[:i]]

    @staticmethod
    def _get_severity(violation_code):
        """Convert a Violation.code to a category"""
        for i in range(4, 0, -1):
            if violation_code[:i] in SEVERITY_CLASSES:
                return SEVERITY_CLASSES[violation_code[:i]]

    def _dictionary_from(self, violation):
        """Convert a Violation to a dictionary."""
        # https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#data-types
        return {
            "type": "issue",
            "check_name": violation.code,
            "description": violation.text,
            "categories": [self._get_category(violation.code)],
            "severity": self._get_severity(violation.code),
            "location": {
                "path": violation.filename,
                "lines": {
                    "begin": violation.line_number,
                }
            },
            "fingerprint": self._fingerprint(violation),
        }

    def format(self, violation):
        """Format a violation."""
        formatted = json.dumps(self._dictionary_from(violation))
        if self.first_written:
            self.write_line(", {}".format(formatted))
        else:
            self.write_line("{}".format(formatted))
            self.first_written = True
